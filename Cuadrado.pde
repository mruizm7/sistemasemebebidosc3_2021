class Cuadrado{
    float lado;
    float xp;
    float yp;
    
    Cuadrado(float a, float b, float c){
       lado = a;
       xp = b;
       yp = c;
       rectMode(CENTER);
    }
    
    public void dibujarCuadrado(){
       rect(xp, yp, lado, lado);
    }
}