Cuadrado c1;
float w;
float arista;

void setup(){
   size(300, 400); // columnas x renglones
   c1 = new Cuadrado(50, width/2, height/2);
   arista = c1.lado;
   w = 0;
   frameRate(20);
}

void draw(){
   background(170);
   c1.dibujarCuadrado();
   c1.lado = arista + 40*sin(w);
   w = w + 0.1;
}
