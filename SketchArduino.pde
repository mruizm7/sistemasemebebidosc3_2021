import processing.serial.*;

Cuadrado c1;
float w;
float arista;

String valor;
String portName;
Serial miPuerto;
int val=0;
int tmp=0;
float c;

void setup(){
   size(300, 400); // columnas x renglones
   c1 = new Cuadrado(50, width/2, height/2);
   portName = Serial.list()[0];
   println(portName);
   miPuerto = new Serial(this, portName, 9600);
   arista = c1.lado;
   w = 0;
   c = 0;
   valor = "";
   frameRate(20);
}

void draw(){
   background(170);
   c1.dibujarCuadrado();
   //c1.lado = arista + 40*sin(w);
   c1.lado = arista +c;
   w = w + 0.1;
   
   valor = miPuerto.readString();
   if(valor != null){
      valor = valor.trim();
      try{
         val = Integer.parseInt(valor);
         tmp = val;
      }
      catch(NumberFormatException e){
          val = tmp;
      }
      
      c = map(val, 0, 1023, -20, 20);
      println(c);
   }  
   
}